local nodejs_scan_job(path) =
  {
    extends: [
      ".sast-analyzer",
      "nodejs-scan-sast",
    ],
    variables: {
      "CI_PROJECT_DIR": path,
      "SECURE_LOG_LEVEL": "debug"
    },
    rules: [
      { "if": "$CI_COMMIT_BRANCH" }
    ],
    artifacts: {
      paths: [
        path + "/gl-sast-report.json"
      ],
      reports: {
        sast: path + "/gl-sast-report.json"
      }
    }
  };

{
  "include": [
    { "template": "SAST.gitlab-ci.yml" }
  ],
  "sast-app1": nodejs_scan_job("app1"),
  "sast-app2": nodejs_scan_job("app2")
}

